/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_LINKS_H
#define NETWORKD_LINKS_H

#include "daemon.h"
#include "link.h"

typedef struct nw_links nw_links;

int nw_links_create(nw_links** links, nw_daemon* daemon);

nw_links* nw_links_ref(nw_links* links);
nw_links* nw_links_unref(nw_links* links);

int nw_links_add_link(nw_links* links, nw_link* link);
void nw_links_drop_link(nw_links* links, nw_link* link);

int nw_links_enumerate(nw_links* links);

nw_link* nw_links_get_by_ifindex(nw_links* links, int ifindex);
nw_link* nw_links_get_by_name(nw_links* links, const char* name);

#endif /* NETWORKD_LINKS_H */
