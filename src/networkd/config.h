/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_CONFIG_H
#define NETWORKD_CONFIG_H

#include <dirent.h>
#include <stdio.h>

#define NETWORK_CONFIG_KEY_MAX_LENGTH		128
#define NETWORK_CONFIG_VALUE_MAX_LENGTH		2048

typedef struct nw_config nw_config;

int nw_config_create(nw_config** config, FILE* f);
int nw_config_open(nw_config** config, const char* path);

nw_config* nw_config_ref(nw_config* config);
nw_config* nw_config_unref(nw_config* config);

int nw_config_copy(nw_config* config, nw_config** copy);

int nw_config_flush(nw_config* config);

int nw_config_read(nw_config* config, FILE* f);
int nw_config_write(nw_config* config, FILE* f);

int nw_config_del(nw_config* config, const char* key);

const char* nw_config_get(nw_config* config, const char* key);
int nw_config_set(nw_config* config, const char* key, const char* value);

int nw_config_get_int(nw_config* config, const char* key, const int __default);
int nw_config_set_int(nw_config* config, const char* key, const int value);

int nw_config_get_bool(nw_config* config, const char* key);
int nw_config_set_bool(nw_config* config, const char* key, const int value);

/*
	Directory
*/

typedef struct nw_configd nw_configd;

int nw_configd_create(nw_configd** dir, const char* path);

nw_configd* nw_configd_ref(nw_configd* dir);
nw_configd* nw_configd_unref(nw_configd* dir);

FILE* nw_configd_fopen(nw_configd* dir, const char* path, const char* mode);
int nw_configd_open_config(nw_config** config, nw_configd* dir, const char* path);
int nw_configd_unlink(nw_configd* dir, const char* path, int flags);

nw_configd* nw_configd_descend(nw_configd* dir, const char* path);

typedef int (*nw_configd_walk_callback)(struct dirent* entry, FILE* f, void* data);

int nw_configd_walk(nw_configd* dir, nw_configd_walk_callback callback, void* data);

/*
	Options
*/

int nw_config_options_read(nw_config* config);
int nw_config_options_write(nw_config* config);

typedef int (*nw_config_option_read_callback_t)
	(nw_config* config, const char* key, void* value, const size_t length, void* data);
typedef int (*nw_config_option_write_callback_t)
	(nw_config* config, const char* key, const void* value, const size_t length, void* data);

int nw_config_option_add(nw_config* config, const char* key, void* value, const size_t length,
	nw_config_option_read_callback_t read_callback,
	nw_config_option_write_callback_t write_callback, void* data);

#define NW_CONFIG_OPTION(config, key, value, length, read_callback, write_callback, data) \
	nw_config_option_add(config, key, value, length, read_callback, write_callback, data)

// String

#define NW_CONFIG_OPTION_STRING(config, key, value) \
	nw_config_option_add(config, key, value, nw_config_read_string, nw_config_write_string, NULL)

int nw_config_read_string(nw_config* config,
	const char* key, void* value, const size_t length, void* data);
int nw_config_write_string(nw_config* config,
	const char* key, const void* value, const size_t length, void* data);

#define NW_CONFIG_OPTION_STRING_BUFFER(config, key, value) \
	nw_config_option_add(config, key, value, sizeof(value), \
		nw_config_read_string_buffer, nw_config_write_string_buffer, NULL)

int nw_config_read_string_buffer(nw_config* config,
	const char* key, void* value, const size_t length, void* data);
#define nw_config_write_string_buffer nw_config_write_string

// String Table

#define NW_CONFIG_OPTION_STRING_TABLE(config, key, value, table) \
	nw_config_option_add(config, key, value, 0, \
		nw_config_read_string_table, nw_config_write_string_table, (void*)table)

int nw_config_read_string_table(nw_config* config,
	const char* key, void* value, const size_t length, void* data);
int nw_config_write_string_table(nw_config* config,
	const char* key, const void* value, const size_t length, void* data);

// Integer

#define NW_CONFIG_OPTION_INT(config, key, value) \
	nw_config_option_add(config, key, value, 0, nw_config_read_int, nw_config_write_int, NULL)

int nw_config_read_int(nw_config* config,
	const char* key, void* value, const size_t length, void* data);
int nw_config_write_int(nw_config* config,
	const char* key, const void* value, const size_t length, void* data);

// Address

#define NW_CONFIG_OPTION_ADDRESS(config, key, value) \
	nw_config_option_add(config, key, value, 0, nw_config_read_address, nw_config_write_address, NULL)

int nw_config_read_address(nw_config* config,
	const char* key, void* value, const size_t length, void* data);
int nw_config_write_address(nw_config* config,
	const char* key, const void* value, const size_t length, void* data);

#endif /* NETWORKD_CONFIG_H */
