/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>

#include "daemon.h"
#include "logging.h"
#include "link.h"
#include "links.h"

struct nw_links_entry {
	nw_link* link;

	// Link to the other entries
	STAILQ_ENTRY(nw_links_entry) nodes;
};

struct nw_links {
	nw_daemon* daemon;
	int nrefs;

	// Link Entries
	STAILQ_HEAD(entries, nw_links_entry) entries;

	// A counter of the link entries
	unsigned int num;
};

int nw_links_create(nw_links** links, nw_daemon* daemon) {
	nw_links* l = calloc(1, sizeof(*l));
	if (!l)
		return 1;

	// Store a reference to the daemon
	l->daemon = nw_daemon_ref(daemon);

	// Initialize the reference counter
	l->nrefs = 1;

	// Initialize entries
	STAILQ_INIT(&l->entries);

	// Reference the pointer
	*links = l;

	return 0;
}

static void nw_links_free(nw_links* links) {
	struct nw_links_entry* entry = NULL;

	while (!STAILQ_EMPTY(&links->entries)) {
		entry = STAILQ_FIRST(&links->entries);

		// Dereference the zone
		nw_link_unref(entry->link);

		// Remove the entry from the list
		STAILQ_REMOVE_HEAD(&links->entries, nodes);

		// Free the entry
		free(entry);
	}

	if (links->daemon)
		nw_daemon_unref(links->daemon);
}

nw_links* nw_links_ref(nw_links* links) {
	links->nrefs++;

	return links;
}

nw_links* nw_links_unref(nw_links* links) {
	if (--links->nrefs > 0)
		return links;

	nw_links_free(links);
	return NULL;
}

static struct nw_links_entry* nw_links_find_link(nw_links* links, const int ifindex) {
	struct nw_links_entry* entry = NULL;

	STAILQ_FOREACH(entry, &links->entries, nodes) {
		if (nw_link_ifindex(entry->link) == ifindex)
			return entry;
	}

	// No match found
	return NULL;
}

int nw_links_add_link(nw_links* links, struct nw_link* link) {
	// Allocate a new entry
	struct nw_links_entry* entry = calloc(1, sizeof(*entry));
	if (!entry)
		return 1;

	// Reference the link
	entry->link = nw_link_ref(link);

	// Add it to the list
	STAILQ_INSERT_TAIL(&links->entries, entry, nodes);

	// Increment the counter
	links->num++;

	return 0;
}

void nw_links_drop_link(nw_links* links, struct nw_link* link) {
	struct nw_links_entry* entry = NULL;

	entry = nw_links_find_link(links, nw_link_ifindex(link));
	if (!entry)
		return;

	DEBUG("Dropping link %d\n", nw_link_ifindex(entry->link));

	STAILQ_REMOVE(&links->entries, entry, nw_links_entry, nodes);
	links->num--;

	// Drop link from all ports
	nw_daemon_ports_walk(links->daemon, __nw_port_drop_link, link);

	// Drop link from all zones
	nw_daemon_zones_walk(links->daemon, __nw_zone_drop_link, link);
}

int nw_links_enumerate(nw_links* links) {
	sd_netlink_message* req = NULL;
	sd_netlink_message* res = NULL;
	int r;

	sd_netlink* rtnl = nw_daemon_get_rtnl(links->daemon);
	if (!rtnl)
		return 1;

	r = sd_rtnl_message_new_link(rtnl, &req, RTM_GETLINK, 0);
	if (r < 0)
		return 1;

	r = sd_netlink_message_set_request_dump(req, 1);
	if (r < 0)
		return 1;

	r = sd_netlink_call(rtnl, req, 0, &res);
	if (r < 0)
		return 1;

	sd_netlink_message* m = res;

	// Iterate through all replies
	do {
		r = nw_link_process(rtnl, m, links->daemon);
		if (r)
			goto ERROR;
	} while ((m = sd_netlink_message_next(m)));

ERROR:
	if (m)
		sd_netlink_message_unref(m);
	if (req)
		sd_netlink_message_unref(req);
	if (res)
		sd_netlink_message_unref(res);

	return r;
}

nw_link* nw_links_get_by_ifindex(nw_links* links, int ifindex) {
	struct nw_links_entry* entry = NULL;

	entry = nw_links_find_link(links, ifindex);
	if (!entry)
		return NULL;

	return nw_link_ref(entry->link);
}

nw_link* nw_links_get_by_name(nw_links* links, const char* name) {
	struct nw_links_entry* entry = NULL;

	STAILQ_FOREACH(entry, &links->entries, nodes) {
		const char* n = nw_link_ifname(entry->link);

		if (strcmp(name, n) == 0)
			return nw_link_ref(entry->link);
	}

	// No match found
	return NULL;
}
