/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <grp.h>
#include <linux/capability.h>
#include <pwd.h>
#include <stddef.h>
#include <sys/capability.h>
#include <sys/prctl.h>
#include <sys/types.h>
#include <unistd.h>

#include "daemon.h"
#include "logging.h"
#include "port.h"

static int cap_acquire_setpcap(void) {
	cap_flag_value_t value;
	int r;

	// Fetch current capabilities
	cap_t caps = cap_get_proc();

	// Check if CAP_SETPCAP is already enabled
	r = cap_get_flag(caps, CAP_SETPCAP, CAP_EFFECTIVE, &value);
	if (r) {
		ERROR("The kernel does not seem to know CAP_SETPCAP: %m\n");
		goto ERROR;
	}

	// It CAP_SETPCAP isn't set, we will try to set it
	if (value != CAP_SET) {
		const cap_value_t cap = CAP_SETPCAP;

		r = cap_set_flag(caps, CAP_EFFECTIVE, 1, &cap, CAP_SET);
		if (r) {
			ERROR("Could not set CAP_SETPCAP: %m\n");
			goto ERROR;
		}

		// Store capabilities
		r = cap_set_proc(caps);
		if (r) {
			ERROR("Could not acquire effective CAP_SETPCAP capability: %m\n");
			goto ERROR;
		}
	}

ERROR:
	if (caps)
		cap_free(caps);

	return r;
}

static cap_flag_value_t keep_cap(const cap_value_t cap, const cap_value_t* keep_caps) {
	for (const cap_value_t* c = keep_caps; *c; c++) {
		if (cap == *c)
			return CAP_SET;
	}

	return CAP_CLEAR;
}

static int drop_capabilities(void) {
	int r;

	const cap_value_t keep_caps[] = {
		CAP_NET_ADMIN,
		CAP_NET_BIND_SERVICE,
		CAP_NET_BROADCAST,
		CAP_NET_RAW,
		0,
	};

	// Acquire CAP_SETPCAP
	r = cap_acquire_setpcap();
	if (r)
		return r;

	// Fetch the current set of capabilities
	cap_t caps = cap_get_proc();

	// Drop all capabilities that we do not need
	for (cap_value_t cap = 1; cap <= CAP_LAST_CAP; cap++) {
		// Skip any capabilities we would like to skip
		cap_flag_value_t flag = keep_cap(cap, keep_caps);

		// Drop the capability from the bounding set
		if (flag == CAP_CLEAR) {
			r = prctl(PR_CAPBSET_DROP, cap);
			if (r) {
				ERROR("Could not drop capability from the bounding set: %m\n");
				goto ERROR;
			}
		}

		r = cap_set_flag(caps, CAP_INHERITABLE, 1, &cap, CAP_CLEAR);
		if (r) {
			ERROR("Could not set capability %d: %m\n", (int)cap);
			goto ERROR;
		}

		r = cap_set_flag(caps, CAP_PERMITTED, 1, &cap, flag);
		if (r) {
			ERROR("Could not set capability %d: %m\n", (int)cap);
			goto ERROR;
		}

		r = cap_set_flag(caps, CAP_EFFECTIVE, 1, &cap, flag);
		if (r) {
			ERROR("Could not set capability %d: %m\n", (int)cap);
			goto ERROR;
		}
	}

	// Restore capabilities
	r = cap_set_proc(caps);
	if (r) {
		ERROR("Could not restore capabilities: %m\n");
		goto ERROR;
	}

ERROR:
	if (caps)
		cap_free(caps);

	return r;
}

static int drop_privileges(const char* user) {
	struct passwd* passwd = NULL;
	int r;

	// Fetch the current user
	uid_t current_uid = getuid();

	// If we have not been launched by root, we will assume that we have already
	// been launched with a minimal set of privileges.
	if (current_uid > 0)
		return 0;

	DEBUG("Dropping privileges...\n");

	// Fetch information about the desired user
	passwd = getpwnam(user);
	if (!passwd) {
		ERROR("Could not find user %s: %m\n", user);
		return 1;
	}

	uid_t uid = passwd->pw_uid;
	gid_t gid = passwd->pw_gid;

	// Change group
	r = setresgid(gid, gid, gid);
	if (r) {
		ERROR("Could not change group to %d: %m\n", gid);
		return 1;
	}

	// Set any supplementary groups
	r = setgroups(0, NULL);
	if (r) {
		ERROR("Could not set supplementary groups: %m\n");
		return 1;
	}

	// Do not drop any capabilities when we change to the new user
	r = prctl(PR_SET_KEEPCAPS, 1);
	if (r) {
		ERROR("Could not set PR_SET_KEEPCAPS: %m\n");
		return 1;
	}

	// Change to the new user
	r = setresuid(uid, uid, uid);
	if (r) {
		ERROR("Could not change user to %d: %m\n", uid);
		return 1;
	}

	// Reset PR_SET_KEEPCAPS
	r = prctl(PR_SET_KEEPCAPS, 0);
	if (r) {
		ERROR("Could not set PR_SET_KEEPCAPS: %m\n");
		return 1;
	}

	// Drop capabilities
	r = drop_capabilities();
	if (r)
		return r;

	return 0;
}

int main(int argc, char** argv) {
	nw_daemon* daemon = NULL;
	int r;

	// Drop privileges
	r = drop_privileges("network");
	if (r)
		return r;

	// Create the daemon
	r = nw_daemon_create(&daemon, argc, argv);
	if (r)
		return r;

	// Run the daemon
	r = nw_daemon_run(daemon);

	// Cleanup
	if (daemon)
		nw_daemon_unref(daemon);

	return r;
}
