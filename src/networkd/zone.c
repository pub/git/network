/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <limits.h>
#include <linux/if_link.h>
#include <stdlib.h>

#include <systemd/sd-bus.h>

#include "config.h"
#include "daemon.h"
#include "link.h"
#include "logging.h"
#include "stats-collector.h"
#include "string.h"
#include "zone.h"

static const nw_string_table_t nw_zone_type_id[] = {
	{ -1, NULL },
};

NW_STRING_TABLE_LOOKUP(nw_zone_type_id_t, nw_zone_type_id)

static void nw_zone_free(nw_zone* zone) {
	if (zone->link)
		nw_link_unref(zone->link);
	if (zone->config)
		nw_config_unref(zone->config);
	if (zone->daemon)
		nw_daemon_unref(zone->daemon);

	free(zone);
}

static int nw_zone_set_link(nw_zone* zone, nw_link* link) {
	// Do nothing if the same link is being re-assigned
	if (zone->link == link)
		return 0;

	// Dereference the former link if set
	if (zone->link)
		nw_link_unref(zone->link);

	// Store the new link
	if (link) {
		zone->link = nw_link_ref(link);

		DEBUG("Zone %s: Assigned link %d\n", zone->name, nw_link_ifindex(zone->link));

	// Or clear the pointer if no link has been provided
	} else {
		zone->link = NULL;

		DEBUG("Zone %s: Removed link\n", zone->name);
	}

	return 0;
}

static int nw_zone_setup(nw_zone* zone) {
	nw_link* link = NULL;
	int r = 0;

	// Find the link
	link = nw_daemon_get_link_by_name(zone->daemon, zone->name);
	if (link) {
		r = nw_zone_set_link(zone, link);
		if (r)
			goto ERROR;
	}

ERROR:
	if (link)
		nw_link_unref(link);

	return r;
}

int nw_zone_create(nw_zone** zone, nw_daemon* daemon, const nw_zone_type_id_t type,
		const char* name, nw_config* config) {
	nw_zone* z = NULL;
	int r;

	// Allocate a new object
	z = calloc(1, sizeof(*z));
	if (!z)
		return -errno;

	// Store a reference to the daemon
	z->daemon = nw_daemon_ref(daemon);

	// Initialize reference counter
	z->nrefs = 1;

	// Store the name
	r = nw_string_set(z->name, name);
	if (r)
		goto ERROR;

	// Copy the configuration
	r = nw_config_copy(config, &z->config);
	if (r < 0)
		goto ERROR;

	// Setup the zone
	r = nw_zone_setup(z);
	if (r)
		goto ERROR;

	*zone = z;
	return 0;

ERROR:
	nw_zone_free(z);
	return r;
}

int nw_zone_open(nw_zone** zone, nw_daemon* daemon, const char* name, FILE* f) {
	nw_config* config = NULL;
	int r;

	// Initialize the configuration
	r = nw_config_create(&config, f);
	if (r < 0)
		goto ERROR;

	// Fetch the type
	const char* type = nw_config_get(config, "TYPE");
	if (!type) {
		ERROR("Zone %s has no TYPE\n", name);
		r = -ENOTSUP;
		goto ERROR;
	}

	// Create a new zone
	r = nw_zone_create(zone, daemon, nw_zone_type_id_from_string(type), name, config);
	if (r < 0)
		goto ERROR;

ERROR:
	if (config)
		nw_config_unref(config);

	return r;
}

nw_zone* nw_zone_ref(nw_zone* zone) {
	zone->nrefs++;

	return zone;
}

nw_zone* nw_zone_unref(nw_zone* zone) {
	if (--zone->nrefs > 0)
		return zone;

	nw_zone_free(zone);
	return NULL;
}

int __nw_zone_drop_port(nw_daemon* daemon, nw_zone* zone, void* data) {
	nw_port* dropped_port = (nw_port*)data;

	// XXX TODO
	(void)dropped_port;

	return 0;
}

int nw_zone_save(nw_zone* zone) {
	nw_configd* configd = NULL;
	FILE* f = NULL;
	int r;

	// Fetch configuration directory
	configd = nw_daemon_configd(zone->daemon, "zones");
	if (!configd) {
		r = -errno;
		goto ERROR;
	}

	// Open file
	f = nw_configd_fopen(configd, zone->name, "w");
	if (!f) {
		r = -errno;
		goto ERROR;
	}

	// Write out the configuration
	r = nw_config_options_write(zone->config);
	if (r < 0)
		goto ERROR;

	// Write the configuration
	r = nw_config_write(zone->config, f);
	if (r < 0)
		goto ERROR;

ERROR:
	if (configd)
		nw_configd_unref(configd);
	if (f)
		fclose(f);
	if (r)
		ERROR("Could not save configuration for zone %s: %s\n", zone->name, strerror(-r));

	return r;
}

const char* nw_zone_name(nw_zone* zone) {
	return zone->name;
}

char* nw_zone_bus_path(nw_zone* zone) {
	char* p = NULL;
	int r;

	// Encode the bus path
	r = sd_bus_path_encode("/org/ipfire/network1/zone", zone->name, &p);
	if (r < 0)
		return NULL;

	return p;
}

int __nw_zone_set_link(nw_daemon* daemon, nw_zone* zone, void* data) {
	nw_link* link = (nw_link*)data;

	// Fetch the link name
	const char* ifname = nw_link_ifname(link);
	if (!ifname) {
		ERROR("Link does not have a name set\n");
		return 1;
	}

	// Set link if the name matches
	if (strcmp(zone->name, ifname) == 0)
		return nw_zone_set_link(zone, link);

	// If we have the link set but the name did not match, we will remove it
	else if (zone->link == link)
		return nw_zone_set_link(zone, NULL);

	return 0;
}

int __nw_zone_drop_link(nw_daemon* daemon, nw_zone* zone, void* data) {
	nw_link* link = (nw_link*)data;

	// Drop the link if it matches
	if (zone->link == link)
		return nw_zone_set_link(zone, NULL);

	return 0;
}

int nw_zone_reconfigure(nw_zone* zone) {
	return 0; // XXX TODO
}

// Carrier

int nw_zone_has_carrier(nw_zone* zone) {
	if (!zone->link)
		return 0;

	return nw_link_has_carrier(zone->link);
}

/*
	MTU
*/
unsigned int nw_zone_mtu(nw_zone* zone) {
	return nw_config_get_int(zone->config, "MTU", NETWORK_ZONE_DEFAULT_MTU);
}

int nw_zone_set_mtu(nw_zone* zone, unsigned int mtu) {
	DEBUG("Change MTU of %s to %u\n", zone->name, mtu);

	return nw_config_set_int(zone->config, "MTU", mtu);
}

/*
	Stats
*/

const struct rtnl_link_stats64* nw_zone_get_stats64(nw_zone* zone) {
	if (!zone->link)
		return NULL;

	return nw_link_get_stats64(zone->link);
}

int __nw_zone_update_stats(nw_daemon* daemon, nw_zone* zone, void* data) {
	nw_link* link = (nw_link*)data;

	// Emit stats if link matches
	if (zone->link == link)
		return nw_stats_collector_emit_zone_stats(daemon, zone);

	return 0;
}

int nw_zone_update_stats(nw_zone* zone) {
	if (zone->link)
		return nw_link_update_stats(zone->link);

	return 0;
}
