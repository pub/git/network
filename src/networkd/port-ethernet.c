/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include "config.h"
#include "json.h"
#include "port.h"
#include "port-ethernet.h"

static int nw_port_ethernet_setup(nw_port* port) {
	int r;

	// Permanent Address
	r = NW_CONFIG_OPTION_ADDRESS(port->config,
		"PERMANENT_ADDRESS", &port->ethernet.permanent_address);
	if (r < 0)
		return r;

	return 0;
}

static int nw_port_ethernet_to_json(nw_port* port, struct json_object* o) {
	char* address = NULL;
	int r = 0;

	// Permanent Address
	address = nw_address_to_string(&port->ethernet.permanent_address);
	if (address) {
		r = json_object_add_string(o, "PermanentAddress", address);
		if (r < 0)
			goto ERROR;
	}

ERROR:
	if (address)
		free(address);

	return r;
}

const nw_port_type_t nw_port_type_ethernet = {
	// Configuration
	.setup = nw_port_ethernet_setup,

	// JSON
	.to_json = nw_port_ethernet_to_json,
};
