/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <linux/veth.h>

#include "json.h"
#include "port.h"
#include "port-veth.h"

static int nw_port_veth_setup(nw_port* port) {
	int r;

	// Peer
	r = NW_CONFIG_OPTION_STRING_BUFFER(port->config, "VETH_PEER", port->veth.peer);
	if (r < 0)
		return 1;

	return 0;
}

static int nw_port_veth_create_link(nw_port* port, sd_netlink_message* m) {
	int r;

	// Open the container
	r = sd_netlink_message_open_container(m, VETH_INFO_PEER);
	if (r < 0)
		return r;

	// Set VETH Peer
	r = sd_netlink_message_append_string(m, IFLA_VLAN_ID, port->veth.peer);
	if (r < 0)
		return r;

	// Close the container
	r = sd_netlink_message_close_container(m);
	if (r < 0)
		return r;

	return 0;
}

static int nw_port_veth_to_json(nw_port* port, struct json_object* o) {
	int r;

	// Add the VETH Peer
	r = json_object_add_string(o, "VETHPeer", port->veth.peer);
	if (r < 0)
		return r;

	return 0;
}

const nw_port_type_t nw_port_type_veth = {
	.kind = "veth",

	// Configuration
	.setup = nw_port_veth_setup,

	// Link
	.create_link = nw_port_veth_create_link,

	// JSON
	.to_json = nw_port_veth_to_json,
};
