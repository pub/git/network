/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <systemd/sd-netlink.h>

#include "config.h"
#include "daemon.h"
#include "logging.h"
#include "port.h"
#include "port-bonding.h"
#include "string.h"

const nw_string_table_t nw_port_bonding_mode[] = {
	{ NW_BONDING_MODE_ROUNDROBIN,   "round-robin" },
	{ NW_BONDING_MODE_ACTIVEBACKUP, "active-backup" },
	{ NW_BONDING_MODE_XOR,          "xor" },
	{ NW_BONDING_MODE_BROADCAST,    "broadcast" },
	{ NW_BONDING_MODE_8023AD,       "802.3ad" },
	{ NW_BONDING_MODE_TLB,          "tlb" },
	{ NW_BONDING_MODE_ALB,          "alb" },
	{ -1, NULL },
};

NW_STRING_TABLE_LOOKUP(nw_port_bonding_mode_t, nw_port_bonding_mode)

static int nw_port_bonding_setup(nw_port* port) {
	int r;

	// Mode
	r = NW_CONFIG_OPTION_STRING_TABLE(port->config,
			"BONDING_MODE", &port->bonding.mode, nw_port_bonding_mode);
	if (r < 0)
		return r;

	return 0;
}

static int nw_port_bonding_create_link(nw_port* port, sd_netlink_message* m) {
	int r;

	// Set mode
	r = sd_netlink_message_append_u8(m, IFLA_BOND_MODE, port->bonding.mode);
	if (r < 0)
		return r;

	return 0;
}

static int nw_port_bonding_to_json(nw_port* port, struct json_object* o) {
	int r;

	// Add mode
	r = json_object_add_string(o, "BondingMode",
		nw_port_bonding_mode_to_string(port->bonding.mode));
	if (r < 0)
		goto ERROR;

ERROR:
	return r;
}

const nw_port_type_t nw_port_type_bonding = {
	.kind = "bond",

	// Configuration
	.setup = nw_port_bonding_setup,

	// Link
	.create_link = nw_port_bonding_create_link,

	// JSON
	.to_json = nw_port_bonding_to_json,
};

const char* nw_port_bonding_get_mode(nw_port* port) {
	return nw_port_bonding_mode_to_string(port->bonding.mode);
}

int nw_port_bonding_set_mode(nw_port* port, const char* mode) {
	const int m = nw_port_bonding_mode_from_string(mode);

	switch (m) {
		case NW_BONDING_MODE_ROUNDROBIN:
		case NW_BONDING_MODE_ACTIVEBACKUP:
		case NW_BONDING_MODE_XOR:
		case NW_BONDING_MODE_BROADCAST:
		case NW_BONDING_MODE_8023AD:
		case NW_BONDING_MODE_TLB:
		case NW_BONDING_MODE_ALB:
			port->bonding.mode = m;
			break;

		default:
			ERROR("%s: Unsupported bonding mode '%s'\n", port->name, mode);
			errno = ENOTSUP;
			return 1;
	}

	return 0;
}
