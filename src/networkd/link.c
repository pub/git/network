/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <linux/if.h>
#include <linux/if_link.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include <systemd/sd-device.h>
#include <systemd/sd-netlink.h>

#include "daemon.h"
#include "json.h"
#include "link.h"
#include "links.h"
#include "logging.h"
#include "string.h"

struct nw_link {
	nw_daemon* daemon;
	int nrefs;

	// Interface Index
	int ifindex;

	// Interface Name
	char ifname[IFNAMSIZ];

	enum nw_link_state {
		NW_LINK_UNKNOWN = 0,
		NW_LINK_DESTROYED,
	} state;

	// Device
	struct sd_device* device;

	// Stats
	struct rtnl_link_stats64 stats64;

	// MTU
	uint32_t mtu;
	uint32_t min_mtu;
	uint32_t max_mtu;

	// Flags
	unsigned int flags;
	uint8_t operstate;
};

static int nw_link_setup_device(nw_link* link) {
	int r;

	// Fetch sd-device
	r = sd_device_new_from_ifindex(&link->device, link->ifindex);
	if (r < 0) {
		ERROR("Could not fetch sd-device for link %d: %s\n", link->ifindex, strerror(-r));
		return r;
	}

	return 0;
}

static void nw_link_free(nw_link* link) {
	DEBUG("Freeing link (ifindex = %d)\n", link->ifindex);

	if (link->device)
		sd_device_unref(link->device);
	if (link->daemon)
		nw_daemon_unref(link->daemon);
}

int nw_link_create(nw_link** link, nw_daemon* daemon, int ifindex) {
	int r;

	// Allocate a new object
	nw_link* l = calloc(1, sizeof(*l));
	if (!l)
		return -errno;

	// Store a reference to the daemon
	l->daemon = nw_daemon_ref(daemon);

	// Initialize the reference counter
	l->nrefs = 1;

	// Store the ifindex
	l->ifindex = ifindex;

	DEBUG("New link allocated (ifindex = %d)\n", l->ifindex);

	r = nw_link_setup_device(l);
	if (r < 0)
		goto ERROR;

	*link = l;
	return 0;

ERROR:
	nw_link_free(l);
	return r;
}

nw_link* nw_link_ref(nw_link* link) {
	link->nrefs++;

	return link;
}

nw_link* nw_link_unref(nw_link* link) {
	if (--link->nrefs > 0)
		return link;

	nw_link_free(link);
	return NULL;
}

/*
	This is a helper function for when we pass a reference to the event loop
	it will have to dereference the link instance later.
*/
static void __nw_link_unref(void* data) {
	nw_link* link = (nw_link*)data;

	nw_link_unref(link);
}

int nw_link_ifindex(nw_link* link) {
	return link->ifindex;
}

const char* nw_link_ifname(nw_link* link) {
	// Return NULL if name isn't set
	if (!*link->ifname)
		return NULL;

	return link->ifname;
}

// Stats

const struct rtnl_link_stats64* nw_link_get_stats64(nw_link* link) {
	return &link->stats64;
}

static int nw_link_call_getlink(nw_link* link,
		int (*callback)(sd_netlink* rtnl, sd_netlink_message* m, void* data)) {
	sd_netlink_message* m = NULL;
	int r;

	sd_netlink* rtnl = nw_daemon_get_rtnl(link->daemon);
	if (!rtnl)
		return 1;

	// Create a new message
	r = sd_rtnl_message_new_link(rtnl, &m, RTM_GETLINK, link->ifindex);
	if (r < 0) {
		ERROR("Could not allocate RTM_GETLINK message: %m\n");
		goto ERROR;
	}

	// Send the message
	r = sd_netlink_call_async(rtnl, NULL, m, callback,
		__nw_link_unref, nw_link_ref(link), -1, NULL);
	if (r < 0) {
		ERROR("Could not send rtnetlink message: %m\n");
		goto ERROR;
	}

ERROR:
	if (m)
		sd_netlink_message_unref(m);

	return r;
}

static int __nw_link_update_stats(sd_netlink* rtnl, sd_netlink_message* m, void* data) {
	nw_link* link = (nw_link*)data;
	int r;

	// Fetch the stats
	r = sd_netlink_message_read(m, IFLA_STATS64, sizeof(link->stats64), &link->stats64);
	if (r < 0)
		return r;

	DEBUG("Link %d: Stats updated\n", link->ifindex);

	// Log stats
	DEBUG("  Packets    : RX: %12llu, TX: %12llu\n",
		link->stats64.rx_packets, link->stats64.tx_packets);
	DEBUG("  Bytes      : RX: %12llu, TX: %12llu\n",
		link->stats64.rx_bytes,   link->stats64.tx_bytes);
	DEBUG("  Errors     : RX: %12llu, TX: %12llu\n",
		link->stats64.rx_errors,  link->stats64.tx_errors);
	DEBUG("  Dropped    : RX: %12llu, TX: %12llu\n",
		link->stats64.rx_dropped, link->stats64.rx_dropped);
	DEBUG("  Multicast  : %llu\n", link->stats64.multicast);
	DEBUG("  Collisions : %llu\n", link->stats64.collisions);

	// Notify ports that stats have been updated
	r = nw_daemon_ports_walk(link->daemon, __nw_port_update_stats, link);
	if (r)
		return r;

	// Notify zones that stats have been updated
	r = nw_daemon_zones_walk(link->daemon, __nw_zone_update_stats, link);
	if (r)
		return r;

	return 0;
}

int nw_link_update_stats(nw_link* link) {
	return nw_link_call_getlink(link, __nw_link_update_stats);
}

// Carrier

int nw_link_has_carrier(nw_link* link) {
	return link->operstate == IF_OPER_UP;
}

static int nw_link_carrier_gained(nw_link* link) {
	return 0; // XXX TODO
}

static int nw_link_carrier_lost(nw_link* link) {
	return 0; // XXX TODO
}

static int nw_link_initialize(nw_link* link) {
	sd_device *device = NULL;
	int r;

	// Fetch device
	r = sd_device_new_from_ifindex(&device, link->ifindex);
	if (r < 0) {
		WARNING("Could not find device for link %d: %s\n", link->ifindex, strerror(-r));
		r = 0;
		goto ERROR;
	}

	// Check if device is initialized
	r = sd_device_get_is_initialized(device);
	switch (r) {
		// Initialized - fallthrough
		case 0:
			break;

		case 1:
			DEBUG("The device has not been initialized, yet\n");
			r = 0;
			goto ERROR;

		default:
			WARNING("Could not check whether device is initialized, ignoring: %s\n",
				strerror(-r));
			goto ERROR;
	}

	// XXX Check renaming?!

	if (link->device)
		sd_device_unref(link->device);

	// Store the device
	link->device = sd_device_ref(device);

	DEBUG("Link %d has been initialized\n", link->ifindex);

ERROR:
	if (device)
		sd_device_unref(device);

	return r;
}

static int nw_link_update_ifname(nw_link* link, sd_netlink_message* message) {
	const char* ifname = NULL;
	int r;

	r = sd_netlink_message_read_string(message, IFLA_IFNAME, &ifname);
	if (r < 0) {
		ERROR("Could not read link name for link %d: %m\n", link->ifindex);
		return 1;
	}

	// Do nothing if the name is already set
	if (strcmp(link->ifname, ifname) == 0)
		return 0;

	// Otherwise update the name
	r = nw_string_set(link->ifname, ifname);
	if (r) {
		ERROR("Could not set link name: %m\n");
		return 1;
	}

	DEBUG("Link %d has been renamed to '%s'\n", link->ifindex, link->ifname);

	// Assign link to ports
	nw_daemon_ports_walk(link->daemon, __nw_port_set_link, link);

	// Assign link to zones
	nw_daemon_zones_walk(link->daemon, __nw_zone_set_link, link);

	return 0;
}

static int nw_link_update_mtu(nw_link* link, sd_netlink_message* message) {
	uint32_t mtu = 0;
	uint32_t min_mtu = 0;
	uint32_t max_mtu = 0;
	int r;

	// Read the MTU
	r = sd_netlink_message_read_u32(message, IFLA_MTU, &mtu);
	if (r < 0) {
		ERROR("Could not read MTU for link %d: %m\n", link->ifindex);
		return r;
	}

	// Read the minimum MTU
	r = sd_netlink_message_read_u32(message, IFLA_MIN_MTU, &min_mtu);
	if (r < 0) {
		ERROR("Could not read the minimum MTU for link %d: %m\n", link->ifindex);
		return r;
	}

	// Read the maximum MTU
	r = sd_netlink_message_read_u32(message, IFLA_MAX_MTU, &max_mtu);
	if (r < 0) {
		ERROR("Could not read the maximum MTU for link %d: %m\n", link->ifindex);
		return r;
	}

	// Set the maximum MTU to infinity
	if (!max_mtu)
		max_mtu = UINT32_MAX;

	// Store min/max MTU
	link->min_mtu = min_mtu;
	link->max_mtu = max_mtu;

	// End here, if the MTU has not been changed
	if (link->mtu == mtu)
		return 0;

	DEBUG("Link %d: MTU has changed to %" PRIu32 " (min: %" PRIu32 ", max: %" PRIu32 ")\n",
		link->ifindex, link->mtu, link->min_mtu, link->max_mtu);

	// Store MTU
	link->mtu = mtu;

	return 0;
}

static int nw_link_update_flags(nw_link* link, sd_netlink_message* message) {
	unsigned int flags = 0;
	uint8_t operstate = 0;
	int r;

	// Fetch flags
	r = sd_rtnl_message_link_get_flags(message, &flags);
	if (r < 0) {
		DEBUG("Could not read link flags: %m\n");
		return 1;
	}

#if 0
	// Fetch operstate
	r = sd_netlink_message_read_u8(message, IFLA_OPERSTATE, &operstate);
	if (r < 1) {
		ERROR("Could not read operstate: %m\n");
		return 1;
	}
#endif

	// End here if there have been no changes
	if (link->flags == flags && link->operstate == operstate)
		return 0;

	// XXX We should log any changes here

	// Fetch current carrier state
	const int had_carrier = nw_link_has_carrier(link);

	// Store the new flags & operstate
	link->flags = flags;
	link->operstate = operstate;

	// Notify if carrier was gained or lost
	if (!had_carrier && nw_link_has_carrier(link)) {
		r = nw_link_carrier_gained(link);
		if (r < 0)
			return r;

	} else if (had_carrier && !nw_link_has_carrier(link)) {
		r = nw_link_carrier_lost(link);
		if (r < 0)
			return r;
	}

	return 0;
}

/*
	This function is called whenever anything changes, so that we can
	update our internal link object.
*/
static int nw_link_update(nw_link* link, sd_netlink_message* message) {
	int r;

	// Update the interface name
	r = nw_link_update_ifname(link, message);
	if (r)
		return r;

	// Update the MTU
	r = nw_link_update_mtu(link, message);
	if (r)
		return r;

	// Update flags
	r = nw_link_update_flags(link, message);
	if (r)
		return r;

	return 0;
}

int nw_link_process(sd_netlink* rtnl, sd_netlink_message* message, void* data) {
	nw_links* links = NULL;
	nw_link* link = NULL;
	const char* ifname = NULL;
	int ifindex;
	uint16_t type;
	int r;

	nw_daemon* daemon = (nw_daemon*)data;

	// Fetch links
	links = nw_daemon_links(daemon);
	if (!links) {
		r = 1;
		goto ERROR;
	}

	// Check if this message could be received
	if (sd_netlink_message_is_error(message)) {
		r = sd_netlink_message_get_errno(message);
		if (r < 0)
			ERROR("Could not receive link message: %m\n");

		goto IGNORE;
	}

	// Fetch the message type
	r = sd_netlink_message_get_type(message, &type);
	if (r < 0) {
		ERROR("Could not fetch message type: %m\n");
		goto IGNORE;
	}

	// Check type
	switch (type) {
		case RTM_NEWLINK:
		case RTM_DELLINK:
			break;

		default:
			ERROR("Received an unexpected message (type %u)\n", type);
			goto IGNORE;
	}

	// Fetch the interface index
	r = sd_rtnl_message_link_get_ifindex(message, &ifindex);
	if (r < 0) {
		ERROR("Could not fetch ifindex: %m\n");
		goto IGNORE;
	}

	// Check interface index
	if (ifindex <= 0) {
		ERROR("Received an invalid ifindex\n");
		goto IGNORE;
	}

	// Fetch the interface name
	r = sd_netlink_message_read_string(message, IFLA_IFNAME, &ifname);
	if (r < 0) {
		ERROR("Received a netlink message without interface name: %m\n");
		goto IGNORE;
	}

	// Try finding an existing link
	link = nw_daemon_get_link_by_ifindex(daemon, ifindex);

	switch (type) {
		case RTM_NEWLINK:
			// If the link doesn't exist, create it
			if (!link) {
				r = nw_link_create(&link, daemon, ifindex);
				if (r) {
					ERROR("Could not create link: %m\n");
					goto ERROR;
				}

				// Initialize the link
				r = nw_link_initialize(link);
				if (r < 0)
					goto ERROR;

				// Add it to the list
				r = nw_links_add_link(links, link);
				if (r)
					goto ERROR;
			}

			// Import any data from the netlink message
			r = nw_link_update(link, message);
			if (r)
				goto ERROR;

			break;

		case RTM_DELLINK:
			if (link)
				nw_links_drop_link(links, link);

			break;
	}

IGNORE:
	r = 0;

ERROR:
	if (links)
		nw_links_unref(links);
	if (link)
		nw_link_unref(link);

	return r;
}

static int __nw_link_destroy(sd_netlink* rtnl, sd_netlink_message* m, void* data) {
	nw_link* link = (nw_link*)data;
	int r;

	// Check if the operation was successful
	r = sd_netlink_message_get_errno(m);
	if (r < 0) {
		ERROR("Could not remove link %d: %m\n", link->ifindex);
		// XXX We should extract the error message

		return 0;
	}

	// Mark this link as destroyed
	link->state = NW_LINK_DESTROYED;

	return 0;
}

int nw_link_destroy(nw_link* link) {
	sd_netlink_message* m = NULL;
	int r;

	sd_netlink* rtnl = nw_daemon_get_rtnl(link->daemon);
	if (!rtnl)
		return 1;

	DEBUG("Destroying link %d\n", link->ifindex);

	// Create a new message
	r = sd_rtnl_message_new_link(rtnl, &m, RTM_DELLINK, link->ifindex);
	if (r < 0) {
		ERROR("Could not allocate RTM_DELLINK message: %m\n");
		goto ERROR;
	}

	// Send the message
	r = sd_netlink_call_async(rtnl, NULL, m, __nw_link_destroy,
		__nw_link_unref, nw_link_ref(link), -1, NULL);
	if (r < 0) {
		ERROR("Could not send rtnetlink message: %m\n");
		goto ERROR;
	}

ERROR:
	if (m)
		sd_netlink_message_unref(m);

	return r;
}

// uevent

static int nw_link_uevent_device_is_renaming(sd_device* device) {
	int r;

	r = sd_device_get_property_value(device, "ID_RENAMING", NULL);
	switch (r) {
		case -ENOENT:
			return 0;

		case 0:
			return 1;

		default:
			return r;
	}
}

int nw_link_handle_uevent(nw_link* link, sd_device* device, sd_device_action_t action) {
	int r;

	// Check if the device is renaming
	r = nw_link_uevent_device_is_renaming(device);
	switch (r) {
		// Not renaming - Fallthrough
		case 0:
			break;

		case 1:
			DEBUG("Device is renaming, skipping initialization\n");
			return 0;

		default:
			ERROR("Could not determine whether the device is being renamed: %s\n",
				strerror(-r));
			return r;
	}

	// We need to remove or replace the stored device as it is now outdated
	if (link->device) {
		sd_device_unref(link->device);

		// If the device has been removed, we remove its reference
		if (action == SD_DEVICE_REMOVE)
			link->device = NULL;

		// Otherwise we just store the new one
		else
			link->device = sd_device_ref(device);
	}

	return 0;
}

// JSON

static int nw_link_device_to_json(nw_link* link, struct json_object* o) {
	const char* driver = NULL;
	const char* vendor = NULL;
	const char* model = NULL;
	int r;

	// Fetch driver
	r = sd_device_get_driver(link->device, &driver);
	if (r < 0 && r != -ENOENT)
		return r;

	// Add driver
	if (driver) {
		r = json_object_add_string(o, "Driver", driver);
		if (r < 0)
			return r;
	}

	// Fetch vendor
	r = sd_device_get_property_value(link->device, "ID_VENDOR_FROM_DATABASE", &vendor);
	if (r < 0) {
		r = sd_device_get_property_value(link->device, "ID_VENDOR", &vendor);
		if (r < 0 && r != -ENOENT)
			return r;
	}

	// Add vendor
	if (vendor) {
		r = json_object_add_string(o, "Vendor", vendor);
		if (r < 0)
			return r;
	}

	// Fetch model
	r = sd_device_get_property_value(link->device, "ID_MODEL_FROM_DATABASE", &model);
	if (r < 0) {
		r = sd_device_get_property_value(link->device, "ID_MODEL", &model);
		if (r < 0 && r != -ENOENT)
			return r;
	}

	// Add model
	if (model) {
		r = json_object_add_string(o, "Model", model);
		if (r < 0)
			return r;
	}

	return 0;
}

int nw_link_to_json(nw_link* link, struct json_object* o) {
	int r;

	// Add ifindex
	r = json_object_add_int64(o, "LinkIndex", link->ifindex);
	if (r < 0)
		return r;

	// Add sd-device stuff
	r = nw_link_device_to_json(link, o);
	if (r < 0)
		return r;

	return 0;
}
