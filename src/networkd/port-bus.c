/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <systemd/sd-bus.h>

#include "address.h"
#include "bus.h"
#include "daemon.h"
#include "json.h"
#include "logging.h"
#include "port.h"
#include "port-bus.h"
#include "ports.h"

static int nw_port_node_enumerator(sd_bus* bus, const char* path, void* data,
		char*** nodes, sd_bus_error* error) {
	int r;

	DEBUG("Enumerating ports...\n");

	// Fetch a reference to the daemon
	nw_daemon* daemon = (nw_daemon*)data;

	// Fetch ports
	nw_ports* ports = nw_daemon_ports(daemon);

	// Make bus paths for all ports
	r = nw_ports_bus_paths(ports, nodes);
	if (r)
		goto ERROR;

ERROR:
	nw_ports_unref(ports);

	return r;
}

static int nw_port_object_find(sd_bus* bus, const char* path, const char* interface,
		void* data, void** found, sd_bus_error* error) {
	char* name = NULL;
	int r;

	// Fetch a reference to the daemon
	nw_daemon* daemon = (nw_daemon*)data;

	// Decode the path of the requested object
	r = sd_bus_path_decode(path, "/org/ipfire/network1/port", &name);
	if (r <= 0)
		return 0;

	// Find the port
	nw_port* port = nw_daemon_get_port_by_name(daemon, name);
	if (!port)
		return 0;

	// Match!
	*found = port;

	nw_port_unref(port);

	return 1;
}

static int nw_port_bus_get_address(sd_bus* bus, const char* path, const char* interface,
		const char* property, sd_bus_message* reply, void* data, sd_bus_error* error) {
	nw_port* port = (nw_port*)data;
	int r;

	// Fetch the address
	const nw_address_t* address = nw_port_get_address(port);

	// Format the address as a string
	char* s = nw_address_to_string(address);
	if (!s) {
		// XXX How to handle any errors?
		return 0;
	}

	// Append the address to the return value
	r = sd_bus_message_append(reply, "s", s);
	if (r)
		goto ERROR;

ERROR:
	if (s)
		free(s);

	return r;
}

static int nw_port_bus_describe(sd_bus_message* message, void* data,
		sd_bus_error* error) {
	sd_bus_message* reply = NULL;
	struct json_object* json = NULL;
	char* text = NULL;
	int r;

	nw_port* port = (nw_port*)data;

	// Export all data to JSON
	r = nw_port_to_json(port, &json);
	if (r < 0)
		goto ERROR;

	// Convert JSON to string
	r = json_to_string(json, &text, NULL);
	if (r < 0)
		goto ERROR;

	// Create a reply message
	r = sd_bus_message_new_method_return(message, &reply);
	if (r < 0)
		goto ERROR;

	r = sd_bus_message_append(reply, "s", text);
	if (r < 0)
		goto ERROR;

	// Send the reply
	r = sd_bus_send(NULL, reply, NULL);

ERROR:
	if (reply)
		sd_bus_message_unref(reply);
	if (text)
		free(text);

	return r;
}

static const sd_bus_vtable port_vtable[] = {
	SD_BUS_VTABLE_START(0),

	// Address
	SD_BUS_PROPERTY("Address", "s", nw_port_bus_get_address,
		0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),

	// Operations
	SD_BUS_METHOD_WITH_ARGS("Describe", SD_BUS_NO_ARGS, SD_BUS_RESULT("s", json),
		nw_port_bus_describe, SD_BUS_VTABLE_UNPRIVILEGED),

	SD_BUS_VTABLE_END
};

const nw_bus_implementation port_bus_impl = {
	"/org/ipfire/network1/port",
	"org.ipfire.network1.Port",
	.fallback_vtables = BUS_FALLBACK_VTABLES({port_vtable, nw_port_object_find}),
	.node_enumerator = nw_port_node_enumerator,
};
