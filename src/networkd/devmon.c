/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <systemd/sd-device.h>

#include "daemon.h"
#include "devmon.h"
#include "logging.h"

static int nw_daemon_handle_uevent_net(nw_daemon* daemon,
		sd_device* device, sd_device_action_t action) {
	nw_link* link = NULL;
	int ifindex;
	int r;

	// Fetch ifindex
	r = sd_device_get_ifindex(device, &ifindex);
	if (r < 0) {
		ERROR("Could not get ifindex from uevent: %s\n", strerror(-r));
		goto ERROR;
	}

	// Fetch the link
	link = nw_daemon_get_link_by_ifindex(daemon, ifindex);
	if (!link) {
		DEBUG("Could not fetch link %d, ignoring\n", ifindex);
		r = 0;
		goto ERROR;
	}

	// Let the link handle its uevent
	r = nw_link_handle_uevent(link, device, action);

ERROR:
	if (link)
		nw_link_unref(link);

	return r;
}

int nw_devmon_handle_uevent(sd_device_monitor* monitor, sd_device* device, void* data) {
	sd_device_action_t action;
	const char* subsystem = NULL;
	int r;

	// Fetch daemon
	nw_daemon* daemon = (nw_daemon*)data;

	// Fetch action
	r = sd_device_get_action(device, &action);
	if (r < 0) {
		WARNING("Could not get uevent action, ignoring: %s\n", strerror(-r));
		return r;
	}

	// Fetch subsystem
	r = sd_device_get_subsystem(device, &subsystem);
	if (r < 0) {
		ERROR("Could not get uevent subsystem, ignoring: %s\n", strerror(-r));
		return r;
	}

	// Handle any links
	if (strcmp(subsystem, "net") == 0) {
		r = nw_daemon_handle_uevent_net(daemon, device, action);

	} else {
		DEBUG("Received an uevent for an unhandled subsystem '%s', ignoring\n", subsystem);
		return 0;
	}

	// Log if something went wrong
	if (r < 0)
		ERROR("Failed processing uevent: %s\n", strerror(-r));

	return 0;
}
