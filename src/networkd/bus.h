/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_BUS_H
#define NETWORKD_BUS_H

#define NETWORKD_BUS_DESCRIPTION		"networkd"

#define DEFAULT_SYSTEM_BUS_ADDRESS		"unix:path=/run/dbus/system_bus_socket"

#include <systemd/sd-bus.h>
#include <systemd/sd-event.h>

#include "daemon.h"

int nw_bus_connect(sd_bus** bus, sd_event* loop, nw_daemon* daemon);

struct nw_bus_vtable_pair {
	const sd_bus_vtable* vtable;
	sd_bus_object_find_t object_find;
};

typedef struct nw_bus_implementation {
	const char* path;
	const char* interface;
	const sd_bus_vtable** vtables;
	const struct nw_bus_vtable_pair* fallback_vtables;
	sd_bus_node_enumerator_t node_enumerator;
	const struct nw_bus_implementation** children;
} nw_bus_implementation;

#define BUS_FALLBACK_VTABLES(...) ((const struct nw_bus_vtable_pair[]) { __VA_ARGS__, {} })
#define BUS_IMPLEMENTATIONS(...) ((const nw_bus_implementation* []) { __VA_ARGS__, NULL })
#define BUS_VTABLES(...) ((const sd_bus_vtable* []){ __VA_ARGS__, NULL })

int nw_bus_register_implementation(sd_bus* bus,
	const nw_bus_implementation* impl, void* data);

#endif /* NETWORKD_BUS_H */
