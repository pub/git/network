/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <limits.h>
#include <stdlib.h>

#include <systemd/sd-bus.h>

#include "../networkd/json.h"
#include "../networkd/string.h"
#include "command.h"
#include "port.h"
#include "terminal.h"

typedef int (*networkctl_port_walk_callback)
	(sd_bus* bus, const char* path, const char* name, void* data);

static int networkctl_port_walk(sd_bus* bus,
		networkctl_port_walk_callback callback, void* data) {
	sd_bus_message* reply = NULL;
	sd_bus_error error = SD_BUS_ERROR_NULL;
	int r;

	// Call Listports
	r = sd_bus_call_method(bus, "org.ipfire.network1", "/org/ipfire/network1",
		"org.ipfire.network1", "ListPorts", &error, &reply, "");
	if (r < 0) {
		fprintf(stderr, "ListPorts call failed: %m\n");
		goto ERROR;
	}

	const char* name = NULL;
	const char* path = NULL;

	// Open the container
	r = sd_bus_message_enter_container(reply, 'a', "(so)");
	if (r < 0) {
		fprintf(stderr, "Could not open container: %m\n");
		goto ERROR;
	}

	// Iterate over all ports
	for (;;) {
		r = sd_bus_message_read(reply, "(so)", &name, &path);
		if (r < 0)
			goto ERROR;

		// Break if we reached the end of the container
		if (r == 0)
			break;

		// Call the callback
		r = callback(bus, path, name, data);
		if (r)
			goto ERROR;
	}

	// Close the container
	sd_bus_message_exit_container(reply);

ERROR:
	if (reply)
		sd_bus_message_unref(reply);
	sd_bus_error_free(&error);

	return r;
}

static int networkctl_port_describe(sd_bus* bus, const char* name, char** text) {
	sd_bus_message* reply = NULL;
	sd_bus_error error = SD_BUS_ERROR_NULL;
	char path[PATH_MAX];
	const char* t = NULL;
	int r;

	// Check input
	if (!name || !text)
		return -EINVAL;

	// Make port path
	r = nw_string_format(path, "/org/ipfire/network1/port/%s", name);
	if (r < 0)
		goto ERROR;

	// Call Describe
	r = sd_bus_call_method(bus, "org.ipfire.network1", path,
		"org.ipfire.network1.Port", "Describe", &error, &reply, "");
	if (r < 0) {
		fprintf(stderr, "Describe() call failed: %m\n");
		goto ERROR;
	}

	// Read the text
	r = sd_bus_message_read(reply, "s", &t);
	if (r < 0) {
		fprintf(stderr, "Could not parse bus message: %s\n", strerror(-r));
		goto ERROR;
	}

	// Copy text to heap
	*text = strdup(t);
	if (!*text)
		r = -errno;

ERROR:
	if (reply)
		sd_bus_message_unref(reply);

	return r;
}

// Dump

static int networkctl_port_dump(sd_bus* bus, int argc, char* argv[]) {
	char* text = NULL;
	int r;

	if (argc < 1) {
		fprintf(stderr, "Port required\n");
		return -EINVAL;
	}

	// Describe the port
	r = networkctl_port_describe(bus, argv[0], &text);
	if (r < 0)
		return r;

	// Print the text
	printf("%s\n", text);

	if (text)
		free(text);

	return 0;
}

// List

static int __networkctl_port_list(sd_bus* bus, const char* path, const char* name, void* data) {
	printf("%s\n", name);

	return 0;
}

static int networkctl_port_list(sd_bus* bus, int argc, char* argv[]) {
	return networkctl_port_walk(bus, __networkctl_port_list, NULL);
}

// Show

#define SHOW_LINE "    %-12s : %s\n"

static int __networkctl_port_show(sd_bus* bus, const char* path, const char* name, void* data) {
	struct json_object* object = NULL;
	enum json_tokener_error json_error;
	char* describe = NULL;
	int r;

	// Describe this port
	r = networkctl_port_describe(bus, name, &describe);
	if (r < 0)
		goto ERROR;

	// Parse JSON
	object = json_tokener_parse_verbose(describe, &json_error);
	if (!object) {
		fprintf(stderr, "Could not parse port %s: %s\n",
			name, json_tokener_error_desc(json_error));
		return -EINVAL;
	}

	// Show headline
	printf("Port %s%s%s\n", color_highlight(), name, color_reset());

	// Show type
	const char* type = json_object_fetch_string(object, "Type");
	if (type)
		printf(SHOW_LINE, "Type", type);

	// Show address
	const char* address = json_object_fetch_string(object, "Address");
	if (address)
		printf(SHOW_LINE, "Address", address);

	// Show an empty line at the end
	printf("\n");

	// Success!
	r = 0;

ERROR:
	if (describe)
		free(describe);
	if (object)
		json_object_unref(object);

	return r;
}

static int networkctl_port_show(sd_bus* bus, int argc, char* argv[]) {
	switch (argc) {
		case 0:
			return networkctl_port_walk(bus, __networkctl_port_show, NULL);

		case 1:
			return __networkctl_port_show(bus, NULL, argv[0], NULL);

		default:
			fprintf(stderr, "Too many arguments\n");
			return 1;
	}
}

int networkctl_port(sd_bus* bus, int argc, char* argv[]) {
	static const struct command commands[] = {
		{ "dump", 0, networkctl_port_dump },
		{ "list", 0, networkctl_port_list },
		{ "show", 0, networkctl_port_show },
		{ NULL },
	};

	return command_dispatch(bus, commands, argc, argv);
}
