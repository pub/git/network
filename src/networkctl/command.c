/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <getopt.h>
#include <string.h>

#include "command.h"

static const struct command* command_find(const struct command* commands, const char* verb) {
	for (const struct command* command = commands; command->verb; command++) {
		if (strcmp(command->verb, verb) == 0)
			return command;
	}

	return NULL;
}

int command_dispatch(sd_bus* bus, const struct command* commands, int argc, char* argv[]) {
	const struct command* command = NULL;

	if (!argc) {
		fprintf(stderr, "Command required\n");
		return -EINVAL;
	}

	const char* verb = argv[0];

	// Find a matching command
	command = command_find(commands, verb);
	if (!command) {
		fprintf(stderr, "Unknown command '%s'\n", verb);
		return -EINVAL;
	}

	return command->callback(bus, argc - 1, argv + 1);
}
