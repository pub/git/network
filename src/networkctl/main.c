/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <systemd/sd-bus.h>

#include "command.h"
#include "port.h"
#include "zone.h"

static int networkctl_status(sd_bus* bus, int argc, char* argv[]) {
	printf("%s called\n", __FUNCTION__);
	return 0;
}

static int networkctl_main(sd_bus* bus, int argc, char* argv[]) {
	static const struct command commands[] = {
		{ "port",   0, networkctl_port },
		{ "status", 0, networkctl_status },
		{ "zone",   0, networkctl_zone },
		{ NULL },
	};

	return command_dispatch(bus, commands, argc, argv);
}

static void version(void) {
	printf("networkctl %s\n", PACKAGE_VERSION);

	exit(0);
}

static void help(void) {
	printf(
		"%s [OPTIONS...] COMMAND\n\n"
		"Options:\n"
		"  -h --help          Show help\n"
		"     --version       Show version\n",
		program_invocation_short_name
	);

	exit(0);
}

static int parse_argv(int argc, char* argv[]) {
	enum {
		ARG_VERSION,
	};

	static const struct option options[] = {
		{ "help",    no_argument, NULL, 'h' },
		{ "version", no_argument, NULL, ARG_VERSION },
		{ NULL },
	};
	int c;

	for (;;) {
		c = getopt_long(argc, argv, "h", options, NULL);
		if (c < 0)
			break;

		switch (c) {
			case 'h':
				help();

			case ARG_VERSION:
				version();

			case '?':
				return -EINVAL;

			default:
				break;
		}
	}

	return 0;
}

int main(int argc, char* argv[]) {
	sd_bus* bus = NULL;
	int r;

	// Parse command line arguments
	r = parse_argv(argc, argv);
	if (r)
		goto ERROR;

	// Connect to system bus
	r = sd_bus_open_system(&bus);
	if (r < 0) {
		fprintf(stderr, "Could not connect to system bus: %m\n");
		goto ERROR;
	}

	// Run a command
	r = networkctl_main(bus, argc - 1, argv + 1);

ERROR:
	if (bus)
		sd_bus_flush_close_unref(bus);

	return r;
}
