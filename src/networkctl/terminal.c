/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdlib.h>
#include <unistd.h>

#include "terminal.h"

// Cache the color mode
static color_mode_t __color_mode = COLORS_UNKNOWN;

static color_mode_t detect_color_mode(void) {
	const char* s = NULL;

	// Check for NO_COLOR and if found turn off colours
	s = secure_getenv("NO_COLOR");
	if (s)
		return COLORS_OFF;

	// Disable colours if this isn't an interactive terminal
	if (!isatty(STDIN_FILENO) || !isatty(STDOUT_FILENO) || !isatty(STDERR_FILENO))
		return COLORS_OFF;

	// Otherwise we enable colours
	return COLORS_ON;
}

color_mode_t color_mode() {
	if (__color_mode == COLORS_UNKNOWN) {
		__color_mode = detect_color_mode();
	}

	return __color_mode;
}
