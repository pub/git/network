#!/bin/bash
###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

# Break if anything fails
set -e

# Turn on job control
set -o monitor

run_script() {
	local script="${1}"
	shift

	if [ -f "${script}" ]; then
		echo "Launching ${script}..."

		# Launch the script in a separate shell and echo every command
		if ! ${SHELL} -xe "${script}"; then
			echo "${script} failed" >&2
			return 1
		fi
	fi

	return 0
}

dump_command() {
	echo "Output of $@"

	# Run the command
	$@ 2>&1

	echo "EOF"
}

dump_status() {
	dump_command "printenv"
	dump_command "ps aux"
	dump_command "ip -d link"
}

# Launches networkd in the background
launch_networkd() {
	echo "Launching networkd..."

	# Launch it!
	coproc networkd { ./networkd "$@"; }

	echo "networkd launched as PID ${networkd_PID}"

	# Wait until networkd is initialized
	# XXX Calling sleep(8) is very racy and should be replaced by something that
	# waits until networkd has connected to dbus
	sleep 1
}

terminate_networkd() {
	local seconds=0

	if [ -n "${networkd_PID}" ]; then
		while [ -n "${networkd_PID}" ] && kill -0 "${networkd_PID}"; do
			case "${seconds}" in
				# Send SIGTERM in the beginning
				0)
					echo "Sending SIGTERM to networkd"
					kill -TERM "${networkd_PID}"
					;;

				# After 5 seconds, send SIGKILL
				5)
					echo "Sending SIGKILL to networkd"
					kill -KILL "${networkd_PID}"

					# It is an error, if we have to kill networkd
					exit 1
					;;
			esac

			# Wait for a moment
			sleep 1

			# Increment seconds
			(( seconds++ ))
		done

		echo "networkd has terminated"
	fi
}

# Collect some status information
trap dump_status EXIT

main() {
	local test="${1}"
	shift

	echo "Running ${test}..."

	# Check if the test exists
	if [ ! -d "${test}" ]; then
		echo "Test '${test}' does not exist" >&2
		return 2
	fi

	# Run prepare script
	if ! run_script "${test}/prepare.sh"; then
		return 1
	fi

	# Launch networkd
	launch_networkd --config="${test}/config"

	# Run test script
	if ! run_script "${test}/test.sh"; then
		return 1
	fi

	# Terminate networkd
	terminate_networkd

	# Run cleanup script
	if ! run_script "${test}/cleanup.sh"; then
		return 1
	fi

	return 0
}

# Call main()
main "$@" || exit $?
